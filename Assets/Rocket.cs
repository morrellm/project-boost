﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    AudioSource audioSource;
    [SerializeField]AudioClip mainEngine;
    [SerializeField] AudioClip explosion;
    [SerializeField] AudioClip victory;

    Dictionary<AudioClip, float> clipSpecificVolumes = new Dictionary<AudioClip, float>();

    Rigidbody rigidBody;
    
    Camera mainCamera;
    bool attenuateRocketBoost = false;

    enum State { Alive, Dead, LevelComplete };
    State state = State.Alive;

    [SerializeField]
    protected float rotationThrust = 75.0f;

    [SerializeField]
    protected float mainThrust = 20.0f;

    [SerializeField]
    protected float deathResetDelaySeconds = 1f;

    [SerializeField]
    protected float goToNextLevelDelaySeconds = 5f;

    [SerializeField]
    protected ParticleSystem thrustPartSys;

    [SerializeField]
    protected ParticleSystem explostionPartSys;

    [SerializeField]
    protected ParticleSystem victoryPartSys;


    #region  DEBUG
    bool collisionsEnabled = true;
    #endregion

    // Use this for initialization
    void Start ()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        
        mainCamera = Camera.main;

        if (mainEngine != null)
        {
            //clipSpecificVolumes.Add(mainEngine, 1f);
        }
    }



    // Update is called once per frame
    void Update ()
    {
        ProcessAudioInput();
        ProcessInput();
        if (Debug.isDebugBuild) ProcessDebugInput();
        ProcessParticleInput();
        CameraFollow();
        ProcessAudioAttenuation();
	}

    void ProcessDebugInput()
    {
        if (Input.GetKeyUp(KeyCode.L))
        {
            LevelComplete();
        }

        if (Input.GetKeyUp(KeyCode.C))
        {
            collisionsEnabled = !collisionsEnabled;
            print("Collisions " + (collisionsEnabled ? "en" : "dis") + "abled!");
        }
    }

    private void ProcessParticleInput()
    {
        if (Input.GetKey(KeyCode.Space) && CanControl())
        {
            if (!thrustPartSys.isPlaying) thrustPartSys.Play();
        }
        else
        {
            thrustPartSys.Stop();
        }
    }

    private void CameraFollow()
    {
        if (mainCamera != null && mainCamera.isActiveAndEnabled)
        {
            var diff = -mainCamera.transform.rotation.eulerAngles.z;
            Camera.main.transform.Rotate(
                Vector3.forward,
                diff
            );
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (state != State.Alive) return;

        switch(collision.gameObject.tag)
        {
            case "Friendly":
                break;
            case "Finish":
                OnVictory();
                break;
            default:
                if (collisionsEnabled) OnDeath();
                break;
        }
    }

    private void OnVictory()
    {
        print("Level Complete!");
        state = State.LevelComplete;
        PlayAudioClip(victory);
        victoryPartSys.Play();
        Invoke("LevelComplete", goToNextLevelDelaySeconds);
    }

    private void OnDeath()
    {
        print("Ouch!");
        state = State.Dead;
        PlayAudioClip(explosion);
        explostionPartSys.Play();
        Invoke("Dead", deathResetDelaySeconds);
    }

    void LevelComplete()
    {
        var curScene = SceneManager.GetActiveScene();
        var nextSceneIndex = curScene.buildIndex + 1;
        if (nextSceneIndex >= SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
        }

        SceneManager.LoadScene(nextSceneIndex);
    }

    void Dead()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void ProcessAudioAttenuation()
    {
        if (attenuateRocketBoost)
        {
            audioSource.volume *= 0.9f;

            if (audioSource.volume <= 0.005)
            {
                ResetThrustAudioAttenuation();
            }
        }
    }

    private void ResetThrustAudioAttenuation()
    {
        audioSource.Stop();
        audioSource.volume = 1f;
        
        attenuateRocketBoost = false;
    }

    bool CanControl()
    {
        return state == State.Alive;
    }

    void ProcessInput()
    {
        if (!CanControl()) return;
        ProcessThrustInput();
        ProcessRotationInput();
    }

    private void ProcessRotationInput()
    {
        var rotationThrust = this.rotationThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            RotateManually(rotationThrust);
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            RotateManually(-rotationThrust);
        }
    }

    void ProcessThrustInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            rigidBody.AddRelativeForce(Vector3.up * mainThrust * Time.deltaTime, ForceMode.Force);
            
        }
    }

    void RotateManually(float amount)
    {
        rigidBody.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionZ;//takes control from the physics engine over rotation
        transform.Rotate(Vector3.forward, amount);
        rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
    }

    void ProcessAudioInput()
    {
        if (Input.GetKey(KeyCode.Space) && CanControl())
        {
            if (!audioSource.isPlaying || attenuateRocketBoost)
            {
                PlayAudioClip(mainEngine);
            }
        }
        else if (audioSource.isPlaying && audioSource.clip == mainEngine)
        {
            attenuateRocketBoost = true;
            //audioSource.Stop();
        }
    }

    void PlayAudioClip(AudioClip ac)
    {
        ResetThrustAudioAttenuation();

        if (clipSpecificVolumes.ContainsKey(ac))
        {
            audioSource.volume = clipSpecificVolumes[ac];
        }

        audioSource.clip = ac;
        audioSource.Play();
    }
}
