﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour {

    [SerializeField]
    Vector3 movementVector = new Vector3(0, 0, 0);

    [SerializeField]
    [Range(0, 1)]
    float oscillationPercentage = 0f;

    [SerializeField]
    float periodInSeconds = 5f;

    public float Period { get { return periodInSeconds; } }

    [SerializeField]
    bool IsMoving = true;

    enum MotionType { Linear, Sinusoidal };

    [SerializeField]
    MotionType motionType = MotionType.Linear;

    protected float oscillationDirection = 1;

    protected Vector3 initialPostion;

	// Use this for initialization
	void Start ()
    {
        initialPostion = transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (IsMoving && periodInSeconds >= Mathf.Epsilon)
        {
            switch (motionType)
            {
                case MotionType.Linear:
                    var step = Time.deltaTime / (periodInSeconds / 2);
                    oscillationPercentage += step * oscillationDirection;

                    if (oscillationPercentage >= 1 || oscillationPercentage <= 0)
                    {
                        oscillationPercentage = Mathf.Clamp(oscillationPercentage, 0, 1);
                        oscillationDirection = -oscillationDirection;
                    }
                    break;
                case MotionType.Sinusoidal:
                    var cycles = Time.time / periodInSeconds;
                    var tau = Mathf.PI * 2f;
                    oscillationPercentage = (Mathf.Sin(cycles * tau) + 1) * 0.5f;
                    break;
            }
        }

        transform.position = initialPostion + (movementVector * oscillationPercentage);
	}
}
